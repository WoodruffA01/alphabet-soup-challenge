//Andrew Woodruff
//woodruffa2@mailbox.winthrop.edu
//864-680-3945

//Enlighten Coding Challenge
//Alphabet Soup
//3-17-2023



//Requirements and creation of command line input interface
const fs = require('fs');
const readline = require('readline');
const rl = readline.createInterface({input : process.stdin, output : process.stdout});

//Initialization of "Global Variables"
var inputData = 0;
var r = -2;
var j = 0;
var word;

var fileName;



//Initializes a static 2d array with Null values to be used in the algorithm (size: 100x100)
let initializeGrid = function(r, c, temp){
    let wordGrid = [];

    for (let i = 0;i < r;i++){
        let row = [];
        for (let j = 0;j < c;j++){
            row.push(temp);
        }
        wordGrid.push(row);
    }

    return wordGrid;
}
let wordGrid = initializeGrid(100,100,null);

//Possible moves for the word search
let moves = [
    [0, 1],//East
    [0, -1],//West
    [-1, 0],//North
    [1, 0],//South
    [-1, 1],//SouthEast
    [-1, -1],//SouthWest
    [1, 1],//NorthEast
    [1,-1]//NorthWest
];


//Searches the Grid of letters for the specified words and returns:
//(The word, the position of the first letter, the position of the last letter)

//An error occurs when the for loop iterates beyond the bounds of the available 2d Array.
//I implimented a try/catch to stop this error since if the error occurs then the loop should stop anyway.
//However, I am not sure how exactly i should have handled the exception.
let wordSearch = function(r, c, wordGrid, moves, word){
    let count = 0;
    let newWord = word.replace(/ /g, "");

    for(let row = 0;row < r;row++){
        for(let col = 0;col < c;col++){
            if(wordGrid[row][col] == newWord.charAt(0)){
                let startRow = row;
                let startCol = col;
                let endRow;
                let endCol;

                //Searches letter grid for starting letter then matches the rest of the word.
                for(let i = 0;i < 8;i++){
                    for(let k = 0;k < newWord.length;k++){
                        try{
                            if(wordGrid[row + (k * moves[i][0])][col + (k * moves[i][1])] == newWord.charAt(k)){
                                count++;
                                posMod = k;
                                endRow = row + (k * moves[i][0]);
                                endCol = col + (k * moves[i][1]);
                            }
                        } catch (error){}
                    }
                    if(count == newWord.length){
                        console.log(word, " ", startRow, ":", startCol," ", endRow, ":", endCol);
                        break;
                    }else{
                        count = 0;
                    }
                }
            }
        }
    }
}


//Prompts the user for file name on command line
rl.question('Please enter the name of the input file: ', (userInput)=>{
    fileName = userInput;
    rl.close();
})

rl.on('close', () => {
    var input = fs.createReadStream('test.txt');
    var rf = readline.createInterface({input : input, terminal : false})

    //Opens specified file and reads its contents line by line
    //Seperates different information using a counter called inputData
    rf.on('line', function(line){
        r++;
        if(inputData == 0){
            for(let i = 0;i < line.length;i++){
                if(line[i] == 'x'){
                    let rowPart = line.slice(0, i);
                    let colPart = line.slice((i + 1), (line.length + 1));
                    row = parseInt(rowPart);
                    col = parseInt(colPart);
                }
            }
            inputData = 1;
        }else if(inputData > 0 && inputData <= row){
            j = 0;
            for (let i = 0;i < line.length;i+=2){
                wordGrid[r][j] =  line.charAt(i);
                j++;
            }
            inputData += 1;
        }else if(inputData > row){
            
            word = line;
            wordSearch(row, col, wordGrid, moves, word);
        }
    })
})